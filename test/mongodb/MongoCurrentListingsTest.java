/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mongodb;

import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Cody
 */
public class MongoCurrentListingsTest {
    
    private static MongoCurrentListings listings;
    
    public MongoCurrentListingsTest() {
        
    }

    @BeforeClass
    public static void startUp() {
        listings = new MongoCurrentListings();
    }
    
    @AfterClass
    public static void shutDown() {
        listings.shutDown();
    }

    @Test
    public void testSession() {

        String session = listings.getSession();
        assertNotNull("This sesson should not be null", session);
        System.out.println("Session is " + session);
    }
    
    @Test
    public void testGetCurrentListings() {

        List<String> jsonList = listings.getCurrentListing("MPLS", "MV");
        assertNotNull("This MPLS list should not be null", jsonList);
        assertFalse("The MPLS list should not be empty", jsonList.isEmpty());
        int size = jsonList.size();
        System.out.println("MPLS list size is " + size);
        System.out.println("last entry is: " + jsonList.get(size-1));
        
        // Repeat for St. Paul
        jsonList = listings.getCurrentListing("STP", "MV");
        assertNotNull("This STP list should not be null", jsonList);
        assertFalse("The STP list should not be empty", jsonList.isEmpty());
        size = jsonList.size();
        System.out.println("SPT list size is " + size);
        System.out.println("last entry is: " + jsonList.get(size-1));
    }
    
}
