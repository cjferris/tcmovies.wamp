/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listener;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Cody
 */
public class TCMoviesWampTestRun {
    
    public TCMoviesWamp wamp = new TCMoviesWamp();
    
    public TCMoviesWampTestRun() {
    }
    
    @Test
    public void testWamp() {
        
        try {
            String[] args = {"ws://127.0.0.1:9021/ws"};
            wamp.initialize(args);
            boolean good = wamp.listenForRequests();
            assertTrue("setting up listenForRequests should be good", good);
            // Wait a bit
            System.out.println("taking a nap");
            Thread.sleep(1000 * 120);
            wamp.closeListener();
            System.out.println("done");
        } catch(Exception nuts) {
            fail("Should not have exception: " + nuts.getMessage());
        }
    }
}
