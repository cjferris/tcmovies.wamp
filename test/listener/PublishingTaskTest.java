/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listener;

import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author Cody
 */
public class PublishingTaskTest {
    
    private TCMoviesWamp wamp;
    private PublishingTask pTask;
    
    public PublishingTaskTest() {
    }
    
    @Before
    public  void startUp() {
       wamp = new TCMoviesWamp();
    }

    @Test
    @Ignore
    public void publishingTestByMC() {
        try {
            RequestJson requestJson = new RequestJson("MPLS", "MV");
            wamp.initialize(new String[] {"ws://127.0.0.1:9021/ws"});
            boolean ok = wamp.listenForRequests();
            assertTrue("Wamp status should be OK", ok);
            pTask = new PublishingTask(wamp.getPClient(), "com.wamp.testing", requestJson);
            pTask.run();
            wamp.closeListener();
            wamp.shutDownNow();
        } catch(Exception ouch) {
            fail("got Exception, not good. " + ouch.getMessage());
        }
    }
    
    @Test
    // @Ignore
    public void publishingTestByTHR() {
        try {
            RequestJson requestJson = new RequestJson("MPLS", "THR");
            wamp.initialize(new String[] {"ws://127.0.0.1:9021/ws"});
            boolean ok = wamp.listenForRequests();
            assertTrue("Wamp status should be OK", ok);
            pTask = new PublishingTask(wamp.getPClient(), "com.wamp.testing", requestJson);
            pTask.run();
            wamp.closeListener();
            wamp.shutDownNow();
        } catch(Exception ouch) {
            fail("got Exception, not good. " + ouch.getMessage());
        }
    }    
}
