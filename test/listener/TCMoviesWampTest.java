/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author Cody
 */
public class TCMoviesWampTest {
    
    public TCMoviesWamp wamp = new TCMoviesWamp();
    
    public TCMoviesWampTest() {
    }
    
    @Test
    // @Ignore
    public void testFindRequestTestOne() {
        ObjectMapper mapper = new ObjectMapper();
        ArrayNode testArray = mapper.createArrayNode();
        ObjectNode testNode = mapper.createObjectNode();
        for (int i = 0; i < TCMoviesWamp.REQUEST_NAMES.length; i++) {
            testNode.put(TCMoviesWamp.REQUEST_NAMES[i], "value-"+i);
        }
        testArray.add(testNode);
        //  Test with empty array
        String json = wamp.findRequest(testArray);
        assertNotNull("Returned json should not be null", json);
        assertFalse("Returned json should not be empty", json.isEmpty());
        assertTrue("Default is not right", json.contains(TCMoviesWamp.REQUEST_NAMES[0]));
        System.out.println("json: " + json);
    }

    @Test
    // @Ignore
    public void testFindRequestTestTwo() {
        ObjectMapper mapper = new ObjectMapper();
        ArrayNode testArray = mapper.createArrayNode();
        ObjectNode testNode = mapper.createObjectNode();
        // Leave last field out.
        for (int i = 0; i < TCMoviesWamp.REQUEST_NAMES.length-1; i++) {
            testNode.put(TCMoviesWamp.REQUEST_NAMES[i], "matter-"+i);
        }
        testArray.add(testNode);
        //  Test with empty array
        String json = wamp.findRequest(testArray);
        assertNotNull("Returned json should not be null", json);
        assertFalse("Returned json should not be empty", json.isEmpty());
        assertTrue("Default is not right", json.contains(TCMoviesWamp.REQUEST_NAMES[0]));
        System.out.println("json: " + json);
    }
    
    @Test
    public void testFindPubKeyWithEmptyNodes() {

        ObjectMapper mapper = new ObjectMapper();
        ArrayNode testArray = mapper.createArrayNode();
        //  Test with empty array
        String json1 = wamp.findPubKey(testArray);
        assertNotNull("Returned json1 should not be null", json1);
        assertFalse("Returned json1 should not be empty", json1.isEmpty());
        assertTrue("Default is not right", json1.contains(TCMoviesWamp.PUB_SUB_JSON_VALUE));
        System.out.println("json: " + json1);
        //  Test with empty node
        ObjectNode testNode = mapper.createObjectNode();        
        testArray.add(testNode);
        String json2 = wamp.findPubKey(testArray);
        assertNotNull("Returned json2 should not be null", json2);
        assertFalse("Returned json2 should not be empty", json2.isEmpty());
        assertTrue("Default is not right", json2.contains(TCMoviesWamp.PUB_SUB_JSON_VALUE));
        System.out.println("json: " + json2);
        assertFalse("The two keys should not be equal", json1.equals(json2));
    }
    
    @Test
    // @Ignore
    public void testFindPubKeyWithMultipleNodes() {
        String testKey = TCMoviesWamp.PUB_SUB_JSON_VALUE + 4;
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode testNode = mapper.createObjectNode();
        testNode.put(TCMoviesWamp.PUB_SUB_JSON_NAME, testKey);
        ArrayNode testArray = mapper.createArrayNode();
        //  Frist test just the one.
        testArray.add(testNode);
        String json = wamp.findPubKey(testArray);
        assertNotNull("Returned json should not be null", json);
        assertTrue("Returned json should match the one passed", json.contains(testKey));
        System.out.println("json: " + json);
        //  Test with added empty node
        ObjectNode blankNode1 = mapper.createObjectNode();
        ObjectNode blankNode2 = mapper.createObjectNode();
        testArray.add(blankNode1);
        testArray.add(testNode);
        testArray.add(blankNode2);
        json = wamp.findPubKey(testArray);
        assertNotNull("Returned json should not be null", json);
        assertTrue("Returned json should match the one passed", json.contains(testKey));
        System.out.println("json: " + json);
    }
    
    @Test
    public void testProcessCommands() {
        // Frist check status
        assertFalse("Shutdown flag should be false", wamp.isShuttingDown());
        wamp.processCommands("{'commands':['whatever', 'shutdown']}");
        assertTrue("Shutdown flag should now be true", wamp.isShuttingDown());
    }
}
