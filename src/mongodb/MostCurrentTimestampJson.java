/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mongodb;

/**
 *
 * @author Cody
 */
public class MostCurrentTimestampJson {
    
    private final String mostcurrentdate;
    
    public MostCurrentTimestampJson(String mostcurrentdate) {
        this.mostcurrentdate = mostcurrentdate;
    }
    
    public String getMostCurrentTimestamp() {
        return mostcurrentdate;
    }
}
