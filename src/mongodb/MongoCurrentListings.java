/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mongodb;

import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;

import java.util.ArrayList;
import java.util.List;
import static java.util.Arrays.asList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;

/**
 *
 * @author Cody
 */
public class MongoCurrentListings {
    
    private MongoClient mongoClient;
    private MongoDatabase mdb;
    private final Gson gson;
    private String session;
    
    private static Logger logger;    
    
    public static final String DEFAULT_ADDRESS = "localhost";
    public static final String DEFAULT_DB_NAME = "tcmovies";
    public static final String DEFAULT_COLLECTION = "tcmovies";
    public static final String JSON_DATA_KEY = "dataList";
    public static final String LOCATION_ID = "locationId";
    public static final String LIST_BY_ID = "listById";
    public static final String TIME_STAMP = "timestamp";
    private static final String MAX = "max";
    public static final int DEFAULT_PORT = 27017;    
    
    public MongoCurrentListings() {
        this(DEFAULT_ADDRESS, DEFAULT_PORT, DEFAULT_DB_NAME);        
    }

    public MongoCurrentListings(String address, int port, String dbName) {
        logger = LogManager.getLogger(MongoCurrentListings.class);
        gson = new Gson();
        initialize(address, port, dbName);
    } 
    
    private void initialize(String address, int port, String dbName) {      
        mongoClient = new MongoClient(address, port);
        mdb = mongoClient.getDatabase(dbName);
        session = DEFAULT_COLLECTION;
    }

    public void shutDown() {
        if (mongoClient != null) {
            mongoClient.close();
        }
    }
    
    public List<String> getCurrentListing(String locationId, String listById) {
        
        List<String> currentListings = new ArrayList<>();
        // Get the most current timestammp
        String maxTimestamp = getMaxDate(locationId);
        logger.info("..getting currentListings for {} with timestamp of {}", locationId, maxTimestamp);
        // Now get the most current with todays date.
        FindIterable<Document> documents = mdb.getCollection(session).find(
            new Document(LOCATION_ID, locationId)
                    .append(LIST_BY_ID, listById)
                    .append(TIME_STAMP, maxTimestamp));
        // loop through the iterable and pull out the data/json
        documents.forEach(new Block<Document>() {
            @Override
            public void apply(final Document document) {
                if (document.containsKey(JSON_DATA_KEY)) {
                    Document dataList = (Document)document.get(JSON_DATA_KEY);
                    currentListings.add(dataList.toJson());
                }
            }
        });
        // Return the time stamp too
        currentListings.add(gson.toJson(new MostCurrentTimestampJson(maxTimestamp)));
        logger.info("..got listing count of " + currentListings.size());
        return currentListings;
    }
    
    //  The getters and setters, if any
    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }
    
    private String getMaxDate(String locationId) {
        // db.tcmovies.aggregate({$group: { _id: "$locationId", max: {$max:"$timestamp"}}})
        String $max = String.format("$%s", MAX);
        Document maxDateTime = mdb.getCollection(session).aggregate(asList(
                                    new Document("$match", new Document("locationId", locationId)),
                                    new Document("$group", new Document("_id", "$locationId")
                                            .append(MAX, new Document($max, "$timestamp"))))).first();
        if (maxDateTime.containsKey(MAX)) {
            return maxDateTime.getString(MAX);
        }
        return null;
    }    
}
