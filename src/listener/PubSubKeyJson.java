/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listener;

/**
 *
 * @author Cody
 */
public class PubSubKeyJson {
   
    protected String pubsubkey;
    protected boolean newKey = false;
    
    public PubSubKeyJson(String value) {
        this.pubsubkey = value;
    }
    
    public PubSubKeyJson(String value, boolean newKey) {
        this.pubsubkey = value;
        this.newKey = newKey;
    }
    
    public boolean isNewKey() {
        return newKey;
    }
}
