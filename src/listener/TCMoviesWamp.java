/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listener;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.node.ArrayNode;

import com.google.gson.Gson;
import java.io.IOException;
import java.net.URI;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import mongodb.MongoCurrentListings;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;

import rx.Scheduler;
import rx.Subscription;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import ws.wamp.jawampa.ApplicationError;
import ws.wamp.jawampa.Request;
import ws.wamp.jawampa.WampClient;
import ws.wamp.jawampa.WampClientBuilder;
import ws.wamp.jawampa.WampRouter;
import ws.wamp.jawampa.WampRouterBuilder;
import ws.wamp.jawampa.connection.IWampConnectorProvider;
import ws.wamp.jawampa.transport.netty.NettyWampClientConnectorProvider;
import ws.wamp.jawampa.transport.netty.SimpleWampWebsocketListener;

/**
 *
 * @author Cody
 */
public class TCMoviesWamp {

    private ExecutorService executor;
    private ExecutorService publisherExecutor;
    private SimpleWampWebsocketListener server;
    private WampRouter router;
    private WampClient rpcClient;
    private WampClient lClient;
    private WampClient pClient;
    private Scheduler rxScheduler;
    private MongoCurrentListings[] mongoClient; 
    private final Gson gson;
    private static int uniqueId = 0;
    private static boolean shuttingDown = false;
    
    //  Subscription objects
    private Subscription eventSubscription;
    private Subscription rpcSubscription;
    
    //  Logging objects
    private static Logger logger;
    private static Marker ohNuts;

    private String routerUri = null;
    private String realm = null;
    private int mongoClientIndex = 0;
    
    public  static final String ROUTER_URI = "ws://tcmovies.today:9021/ws";
    public  static final String REALM = "realm";
    private static final String PUB_SUB_ADDRESS = "com.tvmovies.wamp";
    private static final String RPC_ADDRESS = "com.tvmovies.wamp.rpc";
    private static final String DEFAULT_LOCATION = "MPLS"; 
    private static final String DEFAULT_VIEW_ID = "MV";
    private static final String REQUEST_ERROR_JSON = "{'error':'Unknown or invalid request'}";
    protected static final String PUB_SUB_JSON_NAME = "pubsubkey";
    protected static final String PUB_SUB_JSON_VALUE = PUB_SUB_ADDRESS + ".pubsubkey.";
    protected static final String[] REQUEST_NAMES = {"locationId", "viewId", "keyId"};
    private  static final int NAP_TIME = (1000 * 20);  // 20 seconds.
    private  static final int QUARTER_SECOND = 250;
    private  static final int MAX_MONGO_CLIENTS = 2;
    
    // the constructor //
    public TCMoviesWamp() {
        logger = LogManager.getLogger(TCMoviesWamp.class);
        ohNuts = ohNuts = MarkerManager.getMarker("WE GOT FATAL ERROR");
        gson = new Gson();
    }
    
    public static void main(String[] args) {
        
        TCMoviesWamp tcWamp = new TCMoviesWamp();
        logger.info("launching main now");
        tcWamp.initialize(args);
        tcWamp.listenForRequests();
        while (!shuttingDown) {
            pause(NAP_TIME);
        }
        tcWamp.closeListener();
    }

    private static void pause(long mills) {
        try {
            Thread.sleep(mills);
        } catch(InterruptedException ignore) { }
    }
    
    protected void initialize(String[] args) {
        // First argument is the ROUTER_URL
        if ((args != null) && (args.length > 0)) {
            // TODO
            routerUri = args[0];
            logger.info("setting router uri to {}", routerUri);
        } else {
            logger.info("got no arguments");
            routerUri = ROUTER_URI;
        }
        realm = REALM;
        executor = Executors.newSingleThreadExecutor();
        rxScheduler = Schedulers.from(executor);
        publisherExecutor = Executors.newFixedThreadPool(16);
        for (int i = 0; i < MAX_MONGO_CLIENTS; i++) {
             // mongoClient[i] = null; // TODO new MongoCurrentListings();
        }
    }    
      
    protected boolean listenForRequests() {
        logger.info("in listenForRequests now");
        // Initialize router.
        WampRouterBuilder routerBuilder = new WampRouterBuilder();
        try {
            routerBuilder.addRealm(realm);
            router = routerBuilder.build();
        } catch(ApplicationError bummer) {
            logger.error(ohNuts, "ApplicationError: {}", bummer.getMessage());
            return false;
        }
                
        URI serverUri = URI.create(routerUri);
        IWampConnectorProvider connectorProvider = new NettyWampClientConnectorProvider();
        WampClientBuilder builder = new WampClientBuilder();
        // Initialize server and build the client listener/client. 
        try {
            server = new SimpleWampWebsocketListener(router, serverUri, null);
            server.start();
            builder.withConnectorProvider(connectorProvider)
                   .withUri(routerUri)
                   .withRealm(realm)
                   .withInfiniteReconnects()
                   .withCloseOnErrors(true)
                   .withReconnectInterval(5, TimeUnit.SECONDS);
            rpcClient = builder.build();
            lClient = builder.build();
            pClient = builder.build();
        } catch(Exception ouch) {
            logger.error(ohNuts, "Exception with builder: {}", ouch.getMessage());
            return false;
        }        

        // Set up RPC
        rpcClient.statusChanged()
               .observeOn(rxScheduler)
               .subscribe(new Action1<WampClient.State>() {
                @Override
                public void call(WampClient.State listenerState) {
                    logger.info("RPC client status changed to {}", listenerState);
                    if (listenerState instanceof WampClient.ConnectedState) {
                        // Subcribe to the request topic and receive events
                        rpcSubscription =
                                rpcClient.registerProcedure(RPC_ADDRESS)
                                         .observeOn(rxScheduler)
                                         .subscribe(new Action1<Request>() {
                                    @Override
                                    public void call(Request request) {
                                        logger.info("..got request for RPC listener");
                                        ArrayNode nodes = request.arguments();
                                        // wampPublisher("got array node size of " + nodes.size());
                                        // Check for errors
                                        if ((nodes == null) || (nodes.size() < 1)) {
                                            try {
                                                logger.info(".. .. nodes not good");
                                                request.replyError(new ApplicationError(ApplicationError.INVALID_PARAMETER));
                                            } catch (ApplicationError ignore) { }
                                        } else {
                                            // Looks good so far. 
                                            PubSubKeyJson pubKeyObj = findPubKeyObject(nodes);
                                            request.reply(gson.toJson(pubKeyObj));
                                            // With new keys we need to pause a bit 
                                            // to give client time to start listen. 
                                            if (pubKeyObj.isNewKey()) {
                                                pause(QUARTER_SECOND);
                                            }
                                            RequestJson requestJson = findRequestObject(nodes);
                                            if (requestJson.isValidRequest()) {
                                                publisherExecutor.execute(new PublishingTask(pClient, pubKeyObj.pubsubkey, requestJson));
                                            } else {
                                                wampPublisher(pubKeyObj.pubsubkey, REQUEST_ERROR_JSON);
                                                logger.info("got unknow request: " + nodes.toString());
                                            }
                                        }
                                    }
                                }
                        );
                    }
                }
        });
        
        // SUBSCRIBE - Now subscribe and listen for commands
        lClient.statusChanged()
               .subscribe(new Action1<WampClient.State>() {
                @Override
                public void call(WampClient.State listenerState) {
                    logger.info("listener client status changed to {}", listenerState);
                    if (listenerState instanceof WampClient.ConnectedState) {
                        // Subcribe to the request topic and receive events
                        logger.info("..setting eventSubscription");
                        eventSubscription = 
                                lClient.makeSubscription(PUB_SUB_ADDRESS, String.class)
                                       .subscribe(new Action1<String>() {
                                    @Override
                                    public void call(String msg) {
                                        logger.info("..got event from listening: {}", msg);
                                        processCommands(msg);
                                    }
                                }, new Action1<Throwable>() {
                                    @Override
                                    public void call(Throwable nuts) {
                                        logger.error(ohNuts, "Listener failed to subscribe: " + nuts.getMessage());
                                    }
                                }, new Action0() {
                                    @Override
                                    public void call() {
                                        logger.info("Listener client subscription ended");
                                    }
                                }
                        );
                    }
                }
        });
        
        // Log state changes - For the publisher client just log state changes        
        pClient.statusChanged().subscribe(new Action1<WampClient.State>() {
            @Override
            public void call(WampClient.State publishingState) {
                logger.info("publishing client status changed to {}", publishingState);
            }
        });

        rpcClient.open();
        lClient.open();
        pClient.open();
        return true;
    }
    
    // Override publisher to allow a defualt address.
    protected void wampPublisher(String json) {
        wampPublisher(PUB_SUB_ADDRESS, json);
    }
    
    private void wampPublisher(String address, String json) {
        
        pClient.publish(address, json)
               .subscribe(new Action1<Long>() {
                @Override
                public void call(Long t1) {
                    logger.info("published to client with {}", json);
                }
            }, new Action1<Throwable>() {
                @Override
                public void call(Throwable oops) {
                    logger.error("Error publishing: " + oops.getMessage());
                }
        });        
    }

    private String findValue(JsonParser jsonParser, String name) throws IOException {        
        int throttle = 0;
        while ((jsonParser.nextToken() != JsonToken.END_OBJECT) && (throttle++ < 12)) {
            if (jsonParser.currentToken() == JsonToken.FIELD_NAME) {
                if (jsonParser.getCurrentName().equalsIgnoreCase(name)) {
                    if (jsonParser.nextToken() == JsonToken.VALUE_STRING) {
                        return jsonParser.getValueAsString();
                    }
                }
            }
        }       
        return "";
    }
    
    // Mostly referenced in Unit tests
    protected String findPubKey(ArrayNode nodes) {
       return gson.toJson(findPubKeyObject(nodes)); 
    }
 
    private PubSubKeyJson findPubKeyObject(ArrayNode nodes) {
 
        int i = 0;
        while (i < nodes.size()) {
            if (nodes.get(i).isObject()) {
                JsonParser jsonParser = nodes.get(i).traverse();
                try {
                    String value = findValue(jsonParser, PUB_SUB_JSON_NAME);
                    if (value.startsWith(PUB_SUB_JSON_VALUE)) {
                        //  Return existing PubSubKey, but remove
                        //  if first to shorten the array. 
                        nodes.remove(i);
                        return new PubSubKeyJson(value);
                    }                                               
                } catch (IOException bummer) {
                    logger.error("IO Exception in findPubKeyObject: {}", bummer.getMessage());
                }
            }
            i++;
        }
        // Assume it does not exist if we got this far. Create it. 
        return new PubSubKeyJson(PUB_SUB_JSON_VALUE + uniqueId++, true);
    }
    
    // Mostly referenced in Unit tests
    protected String findRequest(ArrayNode nodes) {
       return gson.toJson(findRequestObject(nodes)); 
    }
    
    private RequestJson findRequestObject(ArrayNode nodes) {
 
        String[] values = new String[REQUEST_NAMES.length];
        int i = 0;
        int j = 0;
        while (i < nodes.size()) {
            if (nodes.get(i).isObject()) {
                JsonParser jsonParser = nodes.get(i).traverse();
                for(j = 0; j < REQUEST_NAMES.length; j++) {
                    try {
                        values[j] = findValue(jsonParser, REQUEST_NAMES[j]);
                    } catch (IOException bummer) {
                        logger.error("IO Exception in findRequestObject: {}", bummer.getMessage());
                    }
                }
            }
            i++;
        }
        // Assume it does not exist if we got this far. Create it. 
        return new RequestJson(values[0], values[1], values[2]);
    }   
 
    protected void processCommands(String msg) {
        CommandsJson commandList;
        try {
            commandList = gson.fromJson(msg, CommandsJson.class);
        } catch(com.google.gson.JsonSyntaxException ex) { 
            // Not a json with commands
            return;
        }
        commandList.commands.forEach(command -> {
            switch (command) {
                case "shutdown":
                    shutDownNow();
                    break;
                case "ping":
                    logger.info("got ping, uniqueId is " + uniqueId);
                    break;                    
                case "dosomething":
                    break;
                default:
                    logger.info("got commands but non defined");
            }
        });
    }
    
    private int nextMongoClient() {
        if (++mongoClientIndex >= MAX_MONGO_CLIENTS) {
            mongoClientIndex = 0;
        }
        return mongoClientIndex;
    }
    
    protected void closeListener() {
        // Close everything
        // Subscriptions, Client, router, and executer
        logger.info("in closeListener");
        if (!rpcSubscription.isUnsubscribed()) {
            rpcSubscription.unsubscribe();
        }
        if (!eventSubscription.isUnsubscribed()) {
            eventSubscription.unsubscribe();
        }
        
        if (rpcClient != null) {
            rpcClient.close().toBlocking().last();
        }        
        if (lClient != null) {
            lClient.close().toBlocking().last();
        }
        if (pClient != null) {
            pClient.close().toBlocking().last();
        }        
        if (server != null) {
            server.stop();
        }
        if (executor != null) {
            executor.shutdown();
        }
        if (router != null) {
            router.close();
        }        
    }

    public boolean isShuttingDown() {
        return shuttingDown;
    }
    
    public void shutDownNow() {
        shuttingDown = true;
        logger.info("shutting down now");
    }
    
    public WampClient getPClient() {
        return pClient;
    }
}
