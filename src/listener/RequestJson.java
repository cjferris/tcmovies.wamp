/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listener;

/**
 *
 * @author Cody
 */
public class RequestJson {
    
    protected String locationId;
    protected String viewId;
    protected String keyId;

    public RequestJson() {
        this.locationId = "";
        this.viewId = "";
        this.keyId = "";
    }    
    
    public RequestJson(String locationId, String viewId) {
        this.locationId = locationId;
        this.viewId = viewId;
        this.keyId = "";
    }
    
    public RequestJson(String locationId, String viewId, String keyId) {
        this.locationId = locationId;
        this.viewId = viewId;
        this.keyId = keyId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getLocationId() {
        return locationId;
    }    
    
    public void setViewId(String viewId) {
        this.viewId = viewId;
    }
    
    public String getViewId() {
        return viewId;
    }    
    
    public void setKeyId(String keyId) {
        this.keyId = keyId;
    }
    
    public String getKeyId() {
        return keyId;
    }    
    
    public void setAllValues(String[] values) {
        this.locationId = values[0];
        this.viewId = values[1];
        this.keyId = values[2];
    }
    
    public boolean isValidRequest() {
        if ((locationId == null) || (viewId == null) ||
             locationId.isEmpty() || viewId.isEmpty()) {
            return false;
        }
        return true;
    }
}
