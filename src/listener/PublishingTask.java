/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listener;

import java.util.ArrayList;
import java.util.List;
import mongodb.MongoCurrentListings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import rx.functions.Action1;
import ws.wamp.jawampa.WampClient;

/**
 *
 * @author Cody
 */
public class PublishingTask implements Runnable {

    private static MongoCurrentListings currentListings;
    private final RequestJson requestJson;
    private final WampClient pClient;
    private final String pubSubKey;
    
    //  Logging objects
    private static Logger logger = null;
    
    public PublishingTask(final WampClient pClient, 
                          final String pubSubKey, final RequestJson requestJson) {
        this.pClient = pClient;
        this.pubSubKey = pubSubKey;
        this.requestJson = requestJson;
        logger  = LogManager.getLogger(PublishingTask.class);
    }
    
    @Override
    public void run() {
        logger.info("fetching current listings now");
        List<String> listings = fetchCurrentListings();
        while (!listings.isEmpty()) {
            wampPublisher(listings.remove(0));
        }
    }
    
    private void wampPublisher(String jsonListing) {
        pClient.publish(pubSubKey, jsonListing);
    } 
    
    private List<String> fetchCurrentListings() {
        
        List<String> listings = new ArrayList<>();
        currentListings = new MongoCurrentListings();
        try {
             listings.addAll(currentListings.getCurrentListing(requestJson.getLocationId(), requestJson.getViewId()));
        } finally {
            currentListings.shutDown();
        }
        return listings;
    }    
    
    protected List<String> fetchMockListings() {
        
        List<String> listings = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            listings.add("Mock testing " + i);
        }
        return listings;
    }
}
